package main

import (
	"fmt"
	"log"
	"net"
	"time"

	"com.grpc.tleu/greet/greetpb"
	"google.golang.org/grpc"
)

//Server with embedded UnimplementedGreetServiceServer
type Server struct {
	greetpb.UnimplementedGreetServiceServer
}

func PrimeFactors(x int) (ans []int) {
	for x%2 == 0 {
		ans = append(ans, 2)
		x = x / 2
	}
	for i := 3; i*i <= x; i = i + 2 {
		for x%i == 0 {
			ans = append(ans, i)
			x = x / i
		}
	}
	// if n is a prime itself
	if x > 2 {
		ans = append(ans, x)
	}
	return ans
}

func Average(x []int) (ans float32) {
	var sum = 0
	for i := 0; i < len(x); i += 1 {
		sum += x[i]
	}
	ans = float32(sum) / float32(len(x))
	return ans
}

func (s *Server) GreetManyTimes(req *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was invoked with %v \n", req)

	ress := int(req.RequestNum)

	res := PrimeFactors(ress)

	for i := range res {
		res := &greetpb.GreetManyTimesResponse{ResponseNum: int32(i)}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
		time.Sleep(time.Second)
	}
	return nil
}

func (s *Server) GreetManyTimes2(req *greetpb.GreetManyTimesRequest, stream greetpb.GreetService_GreetManyTimesServer) error {
	fmt.Printf("GreetManyTimes function was invoked with %v \n", req)

	ress := int(req.RequestNum)

	res := PrimeFactors(ress)

	for i := range res {
		res := &greetpb.GreetManyTimesResponse{ResponseNum: int32(i)}
		if err := stream.Send(res); err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
		time.Sleep(time.Second)
	}
	return nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	greetpb.RegisterGreetServiceServer(s, &Server{})
	log.Println("Server is running on port:50051")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}
