package main

import (
	"com.grpc.tleu/greet/greetpb"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
)

func main() {
	fmt.Println("Hello I'm a client. Enter a number:")
	var num int32
	fmt.Scanf("%d", &num)
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)
	//doGreetingEveryone(c)
	doManyTimesFromServer(c, num)
	os.Exit(1)
}

func doManyTimesFromServer(c greetpb.GreetServiceClient, num int32) {
	ctx := context.Background()
	req := &greetpb.GreetManyTimesRequest{RequestNum: int32(num)}

	stream, err := c.GreetManyTimes(ctx, req)
	if err != nil {
		log.Fatalf("error while calling GreetManyTimes RPC %v", err)
	}
	defer stream.CloseSend()

LOOP:
	for {
		res, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				// we've reached the end of the stream
				break LOOP
			}
			log.Fatalf("error while reciving from GreetManyTimes RPC %v", err)
		}
		log.Printf("response from Server RESULT NUM:%v \n", res.GetResponseNum())
	}

}
